import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';
import { NavbarComponent } from '../common/components/navbar/navbar.component';

const COMPONENTS = [
  NavbarComponent
]
const DIRECTIVES = []
const PROVIDERS = []
const PIPES = []

@NgModule({
  declarations: [
    COMPONENTS,
    DIRECTIVES,
    PIPES
  ],
  exports: [
    MaterialModule,
    COMPONENTS,
    DIRECTIVES,
    PIPES
  ],
  imports: [
    MaterialModule,
    CommonModule
  ],
  providers: [
    PROVIDERS
  ]
})
export class GlobalModule { }
