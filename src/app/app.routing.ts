import { Routes } from "@angular/router";
import { AdminLayoutComponent } from './pages/admin-layout/admin-layout-component';
import { AuthLayoutComponent } from './pages/auth-layout/auth-layout-component';
import { AuthGuard } from './guard/auth.guard';
export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: '',
        component: AdminLayoutComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./pages/admin-layout/dashboard/dashboard.module').then(m => m.DashboardModule)
            }
        ],
        canActivate: [AuthGuard]
    },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [
            {
                path: 'login',
                loadChildren: () => import('./pages/auth-layout/login/login.module').then(m => m.LoginModule)
            },
            {
                path: 'register',
                loadChildren: () => import('./pages/auth-layout/register/register.module').then(m => m.RegisterModule)
            }
        ]
    }
]