import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { GlobalModule } from '../../../modules/global.module';

export const LoginRoutes: Routes = [
  {
    path: '',
    component: LoginComponent
  }
]

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    GlobalModule,
    CommonModule,
    RouterModule.forChild(LoginRoutes)
  ],
  providers: []
})
export class LoginModule { }
