import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component';
import { GlobalModule } from '../../../modules/global.module';

export const RegisterRoutes: Routes = [
  {
    path: '',
    component: RegisterComponent
  }
]

@NgModule({
  declarations: [
    RegisterComponent
  ],
  imports: [
    GlobalModule,
    CommonModule,
    RouterModule.forChild(RegisterRoutes)
  ],
  providers: []
})
export class RegisterModule { }
