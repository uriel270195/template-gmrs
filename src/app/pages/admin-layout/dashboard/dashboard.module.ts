import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from "./dashboard.component";
import { Routes, RouterModule } from '@angular/router';
import { GlobalModule } from '../../../modules/global.module';

export const DashboardRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent
  }
]

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    GlobalModule,
    CommonModule,
    RouterModule.forChild(DashboardRoutes)
  ],
  providers: []
})
export class DashboardModule { }
